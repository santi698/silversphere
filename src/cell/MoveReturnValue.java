package cell;

public enum MoveReturnValue {
	TARGET_REACHED,
	WATER_REACHED,
	UNABLE_TO_MOVE,
	MOVED
}
